<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-thumbsites?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'thumbsites_description' => 'Thumbsites provides a system of site screenshots generated by dedicated servers. The plugin offers tags, filters and templates for displaying the thumbnail of a given site. The plugin also manages a cache to compensate for frequent server unavailability and accelerate display. Configuration is available in the private area.',
	'thumbsites_slogan' => 'A thumbnail for your sites'
);
