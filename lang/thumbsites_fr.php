<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/thumbsites.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Définir comme logo',
	'bouton_rafraichir' => 'Rafraichir la vignette',

	// C
	'cfg_descriptif' => 'Cette page vous permez de configurer le plugin Thumbsites, et, en particulier, de choisir le serveur de vignettes. Pour en savoir plus, consultez la <a href="https://contrib.spip.net/?article2584">documentation sur contrib</a>',
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</a> propose un service gratuit qui ne nécessite pas d’identification.',
	'cfg_inf_choix_serveur' => 'Choisissez le serveur qui fournira les vignettes de vos sites référencés et compléter éventuellement son paramétrage.',
	'cfg_inf_miwim_presentation' => 'Pour utiliser ce service vous devez mettre un lien vers le site <a href="http://thumbs.miwim.fr">Miwim</a>. La présence de ce lien est vérifié périodiquement par un script.',
	'cfg_inf_rotothumb_presentation' => 'Pour utiliser ce service vous ne devez pas vous enregistrer mais vous devez mettre un lien vers le site <a href="http://www.robothumb.com">Robothumb</a>. La présence de ce lien est vérifié périodiquement par leur soin.',
	'cfg_inf_websnapr_presentation' => 'Pour utiliser ce service vous devez être inscrit sur le site <a href="http://www.websnapr.com">Websnapr</a>. La version gratuite permet de traiter de 250.000 captures de site par mois, à un rythme de 80 captures par heure.',
	'cfg_itm_serveur_apercite' => 'Apercite.fr',
	'cfg_itm_serveur_miwim' => 'Miwim',
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ pixels',
	'cfg_lbl_cle' => 'Votre clé',
	'cfg_lbl_duree_cache' => 'Durée du cache (en jours)',
	'cfg_lbl_serveur' => 'Serveur',
	'cfg_lbl_taille_vignette' => 'Taille des vignettes',
	'cfg_lbl_usage' => 'Conditions d’utilisation',
	'cfg_lgd_cache' => 'Cache',
	'cfg_lgd_choix_serveur' => 'Service',
	'cfg_titre_form' => 'Configurer Thumbsites',
	'cfg_titre_page' => 'Thumbsites',
	'credit_1_thumbshot' => 'vignette fournie par @lien@',
	'credit_nb_thumbshot' => 'vignettes fournies par @lien@',

	// T
	'titre_thumbshot_site' => 'VIGNETTE DU SITE'
);
