<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/thumbsites?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Use as logo',
	'bouton_rafraichir' => 'Refresh the thumbnail',

	// C
	'cfg_descriptif' => 'This page lets you configure the Thumbsites plugin, and, in particular, select the thumbnail server. To find out more, see the <a href="https://contrib.spip.net/?article2584">SPIP-Contrib documentation</a>.',
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</a> offers a free service that requires no identification.',
	'cfg_inf_choix_serveur' => 'Select the server that will provide thumbnails of your sites and complete its settings if necessary.',
	'cfg_inf_miwim_presentation' => 'To use this service you need to link to <a href="http://thumbs.miwim.fr">Miwim</a>. The presence of this link is checked periodically by a script.',
	'cfg_inf_rotothumb_presentation' => 'To use this service you don’t need to register but you do need to link to <a href="http://www.robothumb.com">Robothumb</a>. The presence of this link is periodically checked by a script.',
	'cfg_inf_websnapr_presentation' => 'To use this service you must be registered at <a href="http://www.websnapr.com">Websnapr</a>. The free version can process 250,000 site captures per month, at a rate of 80 captures per hour.',
	'cfg_itm_serveur_apercite' => 'Apercite.fr',
	'cfg_itm_serveur_miwim' => 'Miwim',
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ pixels',
	'cfg_lbl_cle' => 'Your key',
	'cfg_lbl_duree_cache' => 'Cache expiration (in days)',
	'cfg_lbl_serveur' => 'Thumbnail server',
	'cfg_lbl_taille_vignette' => 'Thumbnail size',
	'cfg_lbl_usage' => 'Terms of use',
	'cfg_lgd_cache' => 'Cache',
	'cfg_lgd_choix_serveur' => 'Thumbnail service',
	'cfg_titre_form' => 'Configure Thumbsites',
	'cfg_titre_page' => 'Thumbsites',
	'credit_1_thumbshot' => 'Thumbnail provided by @lien@',
	'credit_nb_thumbshot' => 'Thumbnails provided by @lien@',

	// T
	'titre_thumbshot_site' => 'SITE THUMBNAIL'
);
