<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/thumbsites?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_rafraichir' => 'تازه‌سازي صورتك',

	// C
	'cfg_descriptif' => 'اين صفحه به شما اجازه مي‌دهد تا پلاگين تام‌سايتز (Thumbsites) را پيكربندي كنيد، و به ويژه، سرور ريزنش‌ها را انتخاب كنيد. براي اطلاعات بيشتر به اينجا مراجعه كنيد: <a href="http://contrib.spip.net/?article2584">documentation sur contrib</a>', # MODIF
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</a> خدمات رايگاني را پيشنهاد مي‌دهد كه نيازي به شناسايي ندارد.',
	'cfg_inf_rotothumb_presentation' => 'براي استفاده از اين خدمت لازم نيست ثبت نام كنيد اما بايد يك پيوند با سايت <a href="http://www.robothumb.com">Robothumb</a> برقرار كنيد. تدوام اين پيوند به صورت دوره‌اي توسط سرور‌هاي خودشان بررسي مي‌شود. ',
	'cfg_inf_websnapr_presentation' => 'براي استفاده از اين خدمت بايد در سايت <a href="http://www.websnapr.com">Websnapr</a> ثبت نام كنيد. نسخه‌ي رايگان مي‌تواند 250 هزار كاپچبر (گيراندازي) سايت را در طول يك ماه و با آهنگ 80 كاپچر در ساعت پردازش كند. ',
	'cfg_itm_serveur_apercite' => 'Apercite.fr',
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ پيكسل',
	'cfg_lbl_cle' => 'كليد شما',
	'cfg_lbl_duree_cache' => 'دوره‌ي حافظه‌ي نزديك (به روز)',
	'cfg_lbl_taille_vignette' => 'اندازه‌ي ريزنقش‌ها',
	'cfg_lgd_cache' => 'حافظه‌‌ي نزديك',
	'cfg_lgd_choix_serveur' => 'سرور', # MODIF

	// T
	'titre_thumbshot_site' => 'ريزنقش سايت'
);
