<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-thumbsites?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'thumbsites_description' => 'Thumbsites levert een systeem voor screenshots van sites, aangemaakt op toegewezen servers. De plugin bied bakens, filters en modellen om een miniatuurweergave van een bepaalde site te tonen. Deze plugin beheert ook een cache die inspringt wanneer de servers niet bereikbaar zijn en het vertonen van de afbeelding versnelt. Configuratie gebeurt in de privé-ruimte.',
	'thumbsites_slogan' => 'Een miniatuurafbeelding van gerefereerde sites'
);
