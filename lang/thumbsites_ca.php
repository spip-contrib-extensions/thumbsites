<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/thumbsites?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_descriptif' => 'Aquesta pàgina us permet configurar el plugin Thumbsites, i, en particular, escollir el servidor de vinyetes. Per saber-ne més, consulteu la <a href="http://contrib.spip.net/?article2584">documentació a contrib</a>', # MODIF
	'cfg_inf_rotothumb_presentation' => 'Per utilitzar aquest servei heu de registrar-vos però haureu de posar un enllaç cap al lloc <a href="http://www.robothumb.com">Robothumb</a>. La presència d’aquest vincle és verificada periòdicament per la seva cura. ',
	'cfg_inf_websnapr_presentation' => 'Per utilitzar aquest servei heu d’estar registrat al lloc <a href="http://www.websnapr.com">Websnapr</a>. La versió gratuïta permet processar unes 250.000 captures del lloc per mes, a un ritme de 80 captures per hora.',
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ píxels',
	'cfg_lbl_cle' => 'La vostra clau',
	'cfg_lbl_taille_vignette' => 'Mida de les vinyetes ',
	'cfg_lgd_choix_serveur' => 'Servidor', # MODIF

	// T
	'titre_thumbshot_site' => 'VINYETA DEL LLOC'
);
