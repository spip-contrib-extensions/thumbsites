<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/thumbsites?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Definir como logo',
	'bouton_rafraichir' => 'Refrescar la miniatura',

	// C
	'cfg_descriptif' => 'Esta página le permite configurar el plugin Thumbsites, y, en particular, elegir el servidor de miniaturas. Para saber más, consulte la <a href="http://contrib.spip.net/?article2584">documentación sobre contrib</a>', # MODIF
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</a> propone un servicio gratuito que no precisa identificación.',
	'cfg_inf_choix_serveur' => 'Elija el servidor que proporcionará las miniaturas de sus sitios referenciados y complete eventualmente su configuración.',
	'cfg_inf_miwim_presentation' => 'Para utilizar este servicio debe meter un enlace al sitio <a href="http://thumbs.miwim.fr">Miwim</a>. La presencia de este enlace se verifica periódicamente por un script.',
	'cfg_inf_rotothumb_presentation' => 'Para utilizar este servicio no debe registrarse pero debe indicar un enlace al sitio <a href="http://www.robothumb.com">Robothumb</a>. La presencia de este enlace se verifica periódicamente por sí mismo.',
	'cfg_inf_websnapr_presentation' => 'Para utilizar este servicio debe estar registrado en el sitio <a href="http://www.websnapr.com">Websnapr</a>. La versión gratuita permite tratar 250.000 capturas de sitio por mes, a un ritmo de de 80 capturas por hora.',
	'cfg_itm_serveur_apercite' => 'Apercite.fr',
	'cfg_itm_serveur_miwim' => 'Miwim',
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ pixels',
	'cfg_lbl_cle' => 'Su clave',
	'cfg_lbl_duree_cache' => 'Duración de la caché (en días)',
	'cfg_lbl_serveur' => 'Servidor',
	'cfg_lbl_taille_vignette' => 'Tamaño de las miniaturas',
	'cfg_lbl_usage' => 'Condiciones de uso',
	'cfg_lgd_cache' => 'Caché',
	'cfg_lgd_choix_serveur' => 'Servicio',
	'cfg_titre_form' => 'Configurar Thumbsites',
	'cfg_titre_page' => 'Thumbsites',

	// T
	'titre_thumbshot_site' => 'MINIATURA DEL SITIO'
);
