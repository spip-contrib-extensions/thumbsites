<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-thumbsites?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'thumbsites_description' => 'Thumbsites proporciona un sistema de captura de pantalla de sitio generada por los servidores dedicados. El plugin propone etiquetas, filtros y modelos para mostrar la miniatura de un sitio dado. Este plugin genera también una caché que permite paliar las indisponibilidades frecuentes de servidores y acelerar las visualizaciones. Una configuración está disponible en el espacio privado.',
	'thumbsites_slogan' => 'Una miniatura para sus sitios referenciados'
);
