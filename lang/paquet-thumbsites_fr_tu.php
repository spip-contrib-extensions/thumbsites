<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-thumbsites?lang_cible=fr_tu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'thumbsites_description' => 'Thumbsites fournit un système de capture d’écran de site générée par des serveurs dédiés. Le plugin propose des balises, des filtres et des modèles pour afficher la vignette d’un site donné. Ce plugin gère aussi un cache qui permet de pallier les indisponibilités fréquentes des serveurs et d’accélérer les affichages. Une configuration est disponible dans l’espace privé.',
	'thumbsites_slogan' => 'Une vignette pour tes sites référencés'
);
