<?php

/**
 * Plugin Thumbsite
 * Licence GPL
 * 2008-2021
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * url_thumbsite_serveur() pour le serveur apercite
 * definit de la fonction url_thumbshot() exploitant le serveur d'aperçu de robothumb
 *
 * @param string $url_site url du site à consulter
 * @return string url de l'image générée par le serveur
 */
function serveurs_apercite_url_thumbshot_dist($url_site) {
	include_spip('inc/config');
	$taille = lire_config('thumbsites/apercite_taille', '120x90');

	//retourne l'url de la vignette
	return "https://aprc.it/api/{$taille}/{$url_site}";
}

/**
 * Fournit l'url du serveur Apercite à créditer
 *
 * @return string url pour créditer le site serveur
 */
function serveurs_apercite_url_credit() {
	//retourne l'url pour créditer le site serveur
	return 'https://apercite.fr/fr/';
}
