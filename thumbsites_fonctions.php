<?php

/**
 * Plugin Thumbsite
 * Licence GPL
 * 2008-2021
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * #THUMBSHOT{#URL}
 * #THUMBSHOT{#URL,120} // attention : pour compat avec le comportement historique la hauteur est proportionnelle et c'est donc equivalent au 120x* de |balise_img
 * #THUMBSHOT{#URL,120x60}
 * #THUMBSHOT{#URL,@2x}
 * #THUMBSHOT{#URL,120x60,placeholder.jpg}
 *
 * @param $p
 * @return mixed
 */
function balise_THUMBSHOT_dist($p) {

	$_url = interprete_argument_balise(1, $p);
	if (!$_url) {
		$msg = ['zbug_balise_sans_argument', ['balise' => ' THUMBSHOT']];
		erreur_squelette($msg, $p);
	} else {
		$_taille = interprete_argument_balise(2, $p);
		$_defaut = interprete_argument_balise(3, $p);
		$_defaut = ($_defaut ?: "''");

		$p->code = "((\$f = chercher_filtre('balise_img')) ? \$f(sinon(thumbsites_fichier_thumbshot($_url), $_defaut), ''" . ($_taille ? ", is_numeric($_taille) ? $_taille . 'x*' : $_taille" : '') . ") : '')";
	}

	return $p;
}

/**
 * Une fonction pour charger la function thumbshot en fonction du service
 * @param $service
 * @return false|string
 */
function thumbsites_fonction_thumbshot($service) {
	include_spip('serveurs/' . $service);
	if ($fonction_url_thumbshot = charger_fonction("{$service}_url_thumbshot", 'serveurs', true)) {
		return $fonction_url_thumbshot;
	}

	return '';
}

/**
 * @param string $url_site
 * @return string
 */
function thumbsites_url_thumbshot($url_site) {
	if (!$url_site) {
		return '';
	}
	$url_thumbshot = '';

	//determine le serveur de vignettes a utiliser, defaut apercite.fr
	include_spip('inc/config');
	$service = lire_config('thumbsites/serveur', 'apercite');
	if ($fonction_url_thumbshot = thumbsites_fonction_thumbshot($service)) {
		$url_thumbshot = $fonction_url_thumbshot($url_site);
	}

	return $url_thumbshot;
}

/**
 * Detecter les services de screenshot connus et fournis par ce plugin ou d'autres
 * @param string $dummy
 * @return array
 */
function thumbsites_detecter_services_disponibles($dummy = '') {
	static $services;

	if (is_null($services)) {
		$services_candidats = find_all_in_path('serveurs/', '.*\.php$');
		foreach ($services_candidats as $basename => $file) {
			$service = basename($basename, '.php');
			$path = dirname($file) . '/';
			if (
				file_exists($path . 'inc-configurer-thumbsites-' . $service . '.html')
				and thumbsites_fonction_thumbshot($service)
			) {
				$services[$service] = $service;
			}
		}
	}

	return $services;
}


/**
 * Generer un index des thumbshots de site
 * @param string $dir
 */
function thumbsites_creer_index_thumbshots($dir) {
	static $done = false;
	if ($done) {
		return;
	}
	$done = true;
	if (!file_exists($dir . 'index.php')) {
		ecrire_fichier($dir . 'index.php', '<?php
	foreach(glob(\'./*.jpg\') as $i)
		echo "<img src=\'$i\' />\n";
?>');
	}
}

/**
 * Cree/actualise le fichier cache du thumbshot et renvoie le chemin du fichier
 * @param string $url_site
 * @param bool $refresh
 * @return string
 */
function thumbsites_fichier_thumbshot($url_site, $refresh = false) {
	static $nb = 5; // ne pas en charger plus de 5 anciens par tour

	if (!strlen($url_site) or !parse_url($url_site)) {
		return '';
	}

	$dir_tmp = sous_repertoire(_DIR_VAR, 'cache-thumbsites');
	$md5_url = md5(strtolower($url_site));
	$thumb_cache = $dir_tmp . $md5_url . '.jpg';

	if ($refresh and file_exists($thumb_cache)) {
		$ret = supprimer_fichier($thumb_cache);
		spip_log("thumbshot demande de rafraichissement url $url_site file $thumb_cache suppression reussie ? $ret");
	}

	include_spip('inc/filtres');
	include_spip('inc/config');
	$duree = intval(lire_config('thumbsites/duree_cache', 30));

	if (
		$refresh
		or !file_exists($thumb_cache)
		or ((time() - 3600 * 24 * $duree > filemtime($thumb_cache)) and $nb > 0)
	) {
		spip_log("thumbshot refresh url $url_site file $thumb_cache", 'thumbsites' . _LOG_DEBUG);

		$nb--;
		include_spip('inc/distant');
		$url_thumb = thumbsites_url_thumbshot($url_site);

		$filetmp = $thumb_cache . '.tmp';
		$res = recuperer_url($url_thumb, ['file' => $filetmp]);

		if ($res and !empty($res['file'])) {
			spip_log('thumbshot ok pour ' . $url_site . " dans $filetmp", 'thumbsites' . _LOG_DEBUG);

			// si c'est un png, le convertir en jpg
			$a = @getimagesize($thumb_cache);
			if (
				is_array($a)
				and ($a[2] == 3)
			) {
				include_spip('inc/filtres_images_lib_mini');
				$img = _imagecreatefrompng($filetmp);
				_image_imagejpg($img, $thumb_cache);
				@unlink($filetmp);
			}
			else {
				rename($filetmp, $thumb_cache);
			}

			thumbsites_creer_index_thumbshots($dir_tmp);
		}
		else {
			spip_log('Echec thumbshot pour ' . $url_site . " dans $filetmp", 'thumbsites' . _LOG_ERREUR);
		}
	}

	// On verifie si le thumbshot existe en controlant la taille du fichier
	if (@filesize($thumb_cache)) {
		return $thumb_cache;
	} else {
		return '';
	}
}

/**
 * Fournir automatiquement le thumbshot en guise de logo de site si il y en a pas
 * @param array $flux
 * @return array
 */
function thumbsites_quete_logo_objet($flux) {
	if (
		empty($flux['data'])
		and $flux['args']['mode'] === 'on'
		and $flux['args']['objet'] === 'site'
		and $id_syndic = $flux['args']['id_objet']
	) {
		$url_site = sql_getfetsel('url_site', 'spip_syndic', 'id_syndic=' . intval($id_syndic));
		if (
			$url_site = vider_url($url_site, false)
			and $thumb_file = thumbsites_fichier_thumbshot($url_site)
		) {
			$flux['data'] = [
				'chemin' => $thumb_file,
				'timestamp' => filemtime($thumb_file),
			];
		}
	}
	return $flux;
}


/**
 * Boite de configuration des objets.
 *
 * @param array $flux
 * @return array
 */
function thumbsites_afficher_config_objet($flux) {
	$type = $flux['args']['type'];
	if (
		($type == 'site')
		and ($id = intval($flux['args']['id']))
		and ($url = sql_getfetsel('url_site', 'spip_syndic', 'id_syndic=' . sql_quote($id)))
	) {
		include_spip('inc/thumbsites_filtres');
		if ($thumbshot_cache = thumbsites_fichier_thumbshot($url)) {
			if ($taille = @getimagesize($thumbshot_cache)) {
				$flux['data'] .= recuperer_fond(
					'prive/squelettes/navigation/thumbshot',
					[
						'id_objet' => $id,
						'objet' => objet_type($type),
						'thumbshot_cache' => $thumbshot_cache,
						'largeur' => $taille[0],
						'hauteur' => $taille[1],
						'url' => $url,
					]
				);
			}
		}
	}

	return $flux;
}

if (!function_exists('url_thumbsite')) {
	/**
	 * @deprecated
	 */
	function url_thumbsite($url_site) {
        return thumbsites_url_thumbshot($url_site);
	}
}
if (!function_exists('thumbshot')) {
	/**
	 * @deprecated
	 */
	function thumbshot($url_site, $refresh = false) {
        return thumbsites_fichier_thumbshot($url_site, $refresh);
	}
}

function thumbsites_crediter($item = 'thumbsites:credit_1_thumbshot') {

	$credit = '';

	$service = lire_config('thumbsites/serveur', 'apercite');
	include_spip('serveurs/' . $service);
	if ($fonction_url_credit = charger_fonction("{$service}_url_credit", 'serveurs', true)) {
		// On acquiert l'url de crédit du serveur
		$url = $fonction_url_credit();
		// On détermine le nom affché pour le serveur
		$nom = _T('thumbsites:cfg_itm_serveur_' . $service);
		$lien = '<a href="' . $url . '">' . $nom . '</a>';
		// On vérifie que l'item n'est pas la chaine vide
		if (!$item) {
			$item = 'thumbsites:credit_1_thumbshot';
		}

		$credit = _T($item, ['lien' => $lien]);
	}

	return $credit;
}
