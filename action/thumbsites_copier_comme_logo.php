<?php

/**
 * Plugin Thumbsite
 * Licence GPL
 * 2008-2021
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_thumbsites_copier_comme_logo_dist() {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	list($objet, $id_objet, $url_site) = explode('--', $arg, 3);

	if (strpos($url_site, '//') === false) {
		$url_site = base64_decode($url_site);
	}

	include_spip('thumbsites_fonctions');
	if ($thumb_file = thumbsites_fichier_thumbshot($url_site)) {
		spip_log("action_thumbsites_copier_comme_logo_dist $objet #$id_objet file $thumb_file", 'thumbsites' . _LOG_DEBUG);
		include_spip('action/editer_logo');
		logo_modifier($objet, $id_objet, 'on', $thumb_file);
	}
}
